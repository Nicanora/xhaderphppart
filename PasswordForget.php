<?php

include_once 'index.php';
$results["error"] = false;
$results["message"] = [];

$db= ConnexionPDO();

if(isset($_POST)){
    $result['message']="";
    extract($_POST);
    if(!empty($pseudo) && !empty($password)){


        $sql = $db->prepare("SELECT * FROM users WHERE pseudo = :pseudo ");
        $sql->execute([":pseudo" => $pseudo]);
        $row = $sql->fetch(PDO::FETCH_OBJ);

        if($row)
        {
        	$password = password_hash($password, PASSWORD_BCRYPT);
        	$sql = $db->prepare("UPDATE users SET password =:password WHERE pseudo = :pseudo ");
        	$sql->execute([":password" => $password,

        					":pseudo" => $pseudo]);
            $result["message"]="Change success";
       
        }

        else
        {
                $result["message"]="Not existing pseudo";
        }

     }
    
        echo (json_encode($result));
    }
    ?>