<?php
 
include_once 'index.php'; 
$results["error"] = false;
$results["message"] = [];
$db= ConnexionPDO();


 
if(isset($_POST)){
    extract($_POST);
    
    if(!empty($pseudo) && !empty($password)){
 
        $sql = $db->prepare("SELECT * FROM users WHERE pseudo = :pseudo ");
        $sql->execute([":pseudo" => $pseudo]);
        $row = $sql->fetch(PDO::FETCH_OBJ);
        if($row){
            if(password_verify($password, $row->password)){
                $results["error"] = false;
                $results["id"]=$row->id;
                $results["pseudo"]=$row->pseudo;
                $results["email"]=$row->email;
                $results["password"]=$row->password;

                $results['message']="Welcome";
                
                
            }
            else{
                $results["error"] = true;
                $results["message"] = "Incorrect Password";
                
                
 
            }
 
        } else{
            $results["error"] = true;
            $results["message"] = "Incorrect Pseudo";
                
          
 
            }
 
    } else{
        $results["error"] = true;
        $results["message"] = "Field empty";
        
    }
    

    echo (json_encode($results));
    
}
 
 
 
?>
