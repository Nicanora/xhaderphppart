<?php

include_once 'index.php';
$results["error"] = false;
$results["message"] = [];

$db= ConnexionPDO();
if(isset($_POST)){
    $result['message']="";
    extract($_POST);

    $sql = $db->prepare("SELECT * FROM files WHERE id = :id AND sendername = :currentUserName");
    $sql->execute([":id" => $id,
                   ":currentUserName" => $currentUserName]);
    $row = $sql->fetch(PDO::FETCH_OBJ);
    
    if($row)
    {
            $requete=$db->prepare("DELETE FROM files WHERE id= :id");
            $requete->execute([":id" => $id]);
            $result["message"]="Delete success";
       
    }

    else
    {
        $result["message"]="Unable Delete";
    }

    echo (json_encode($result));
}


?>